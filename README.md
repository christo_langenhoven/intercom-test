Prerequisites:
--------------

* Java 8

How to use:
-----------

* After cloning, on Linux/Mac run `./gradlew run`, on Windows, `gradlew.bat run` (not tested)
* The program will attempt to load the `customers.json` file that should be in the same directory, 
and output the result.

Possible Improvements:
----------------------

* Add command line arguments to specify file, as well as latitude and longitude, instead of hardcoding them
* Expand testing to be more exhaustive
* Package app up into Vagrantfile so reviewers don't have to have Java installed to run the app
* If this were a real production app, it would make sense to post metrics on the input lines that failed to parse and
set up alarming on those metrics
