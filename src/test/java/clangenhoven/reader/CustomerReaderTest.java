package clangenhoven.reader;

import clangenhoven.model.Customer;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CustomerReaderTest {

    @Test
    public void convertsInputStreamToCustomers() throws IOException {
        String input =
                "{\"latitude\": \"53.008769\", \"user_id\": 11, \"name\": \"Richard Finnegan\", \"longitude\": \"-6.1056711\"}\n" +
                "{\"latitude\": \"53.1489345\", \"user_id\": 31, \"name\": \"Alan Behan\", \"longitude\": \"-6.8422408\"}";
        InputStream inputStream = new ByteArrayInputStream(input.getBytes());

        List<Customer> customers = new CustomerReader(inputStream).stream()
                .collect(Collectors.toList());

        assertEquals(2, customers.size());
        assertEquals(11, customers.get(0).getUserId());
        assertEquals(31, customers.get(1).getUserId());
    }

    @Test
    public void ignoresMalformedLines() throws IOException {
        String input =
                "{\"latitude\": \"53.008769\", \"user_id\": 11, \"name\": \"Richard Finnegan\", \"longitude\": \"-6.1056711\"}\n" +
                "{\"latitude\": \"53.1489345\", \"user_id\": 31, \"name\": \"Alan Behan\", \"longitude\": \"-6.8422408\"";
        InputStream inputStream = new ByteArrayInputStream(input.getBytes());
        List<Customer> customers = new CustomerReader(inputStream).stream()
                .collect(Collectors.toList());

        assertEquals(1, customers.size());
        assertEquals(11, customers.get(0).getUserId());
    }

    @Test
    public void handlesEmptyInputStream() throws IOException {
        InputStream inputStream = new ByteArrayInputStream(new byte[0]);
        List<Customer> customers = new CustomerReader(inputStream).stream()
                .collect(Collectors.toList());
        assertTrue(customers.isEmpty());
    }
}
