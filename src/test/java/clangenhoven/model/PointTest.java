package clangenhoven.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PointTest {

    @Test
    public void calculatesAngleCorrectly() {
        Point p1 = new Point(0, 0);
        Point p2 = new Point(45, 90);
        assertEquals(90, p1.angleTo(p2), 0.001);

        Point p3 = new Point(0, 180);
        assertEquals(180, p1.angleTo(p3), 0.001);

        Point p4 = new Point(-45, -90);
        assertEquals(180, p2.angleTo(p4), 0.001);
    }

    @Test
    public void calculatesArcLengthCorrectly() {
        Point p1 = new Point(0, 0);
        Point p2 = new Point(0, -180);
        assertEquals(Math.PI, p1.arcLengthTo(p2, 1), 0.001);
        assertEquals(0, p1.arcLengthTo(p1, 10), 0.001);
    }
}
