package clangenhoven;

import clangenhoven.model.Customer;
import clangenhoven.model.Point;
import clangenhoven.reader.CustomerReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Comparator;
import java.util.TreeSet;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class App {

    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);
    private static final Point OFFICE_LOCATION = new Point(53.339428, -6.257664);
    private static final double EARTH_RADIUS = 6.3781e6;
    private static final double THRESHOLD = 1e5;

    private static final Supplier<TreeSet<Customer>> treeSetSupplier = () ->
            new TreeSet<>(Comparator.comparingLong(Customer::getUserId));

    public static void main(String... argv) {
        try (FileInputStream fileInputStream = new FileInputStream("customers.json")) {
            new CustomerReader(fileInputStream).stream()
                    .filter(c -> c.getLocation().arcLengthTo(OFFICE_LOCATION, EARTH_RADIUS) < THRESHOLD)
                    .collect(Collectors.toCollection(treeSetSupplier))
                    .forEach(System.out::println);
        } catch (IOException e) {
            LOGGER.error("Failed to read from file", e);
        }
    }
}
