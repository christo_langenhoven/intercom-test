package clangenhoven.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import net.jcip.annotations.Immutable;

/**
 * Represents a Customer. Can be de/serialized to and from JSON.
 */
@Immutable
public class Customer {

    private long userId;
    private double latitude;
    private double longitude;
    private String name;

    public Customer() {
    }

    @JsonProperty("user_id")
    public long getUserId() {
        return userId;
    }

    @JsonProperty
    public double getLatitude() {
        return latitude;
    }

    @JsonProperty
    public double getLongitude() {
        return longitude;
    }

    @JsonProperty
    public String getName() {
        return name;
    }

    @JsonIgnore
    public Point getLocation() {
        return new Point(latitude, longitude);
    }

    @Override
    public String toString() {
        return "User ID: " + userId + ", Name: " + name;
    }
}
