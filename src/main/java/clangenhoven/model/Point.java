package clangenhoven.model;

import net.jcip.annotations.Immutable;

import static java.lang.Math.abs;
import static java.lang.Math.acos;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.toDegrees;
import static java.lang.Math.toRadians;

/**
 * Represents a point on a sphere with a latitude and longitude
 */
@Immutable
public class Point {

    private final double latitude;
    private final double longitude;

    private final double radLatitude;
    private final double radLongitude;

    /**
     * @param latitude  Latitude in degrees
     * @param longitude Longitude in degrees
     */
    public Point(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.radLatitude = toRadians(latitude);
        this.radLongitude = toRadians(longitude);
    }

    private double radAngleTo(Point p) {
        return acos(
                sin(radLatitude) * sin(p.radLatitude) + cos(radLatitude) * cos(p.radLatitude) * cos(abs(radLongitude - p.radLongitude))
        );
    }

    /**
     * Get the angle between this point and the provided point in degrees
     */
    public double angleTo(Point p) {
        return toDegrees(radAngleTo(p));
    }

    /**
     * Get the length of the arc between this point and the provided point on a sphere with the provided radius
     */
    public double arcLengthTo(Point p, double radius) {
        return radius * radAngleTo(p);
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
