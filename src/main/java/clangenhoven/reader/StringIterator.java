package clangenhoven.reader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;

class StringIterator implements Iterator<String> {

    private static final Logger LOGGER = LoggerFactory.getLogger(StringIterator.class);

    private BufferedReader bufferedReader;
    private String nextLine;

    public StringIterator(BufferedReader bufferedReader) throws IOException {
        this.bufferedReader = bufferedReader;
        if (this.bufferedReader.ready()) {
            nextLine = this.bufferedReader.readLine();
        }
    }

    @Override
    public boolean hasNext() {
        return nextLine != null;
    }

    @Override
    public String next() {
        if (nextLine == null) {
            throw new NoSuchElementException();
        }
        String line = nextLine;
        try {
            nextLine = bufferedReader.readLine();
        } catch (IOException e) {
            LOGGER.error("Failed to read next line from reader", e);
            nextLine = null;
        }
        return line;
    }
}
