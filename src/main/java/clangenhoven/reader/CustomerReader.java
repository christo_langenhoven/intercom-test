package clangenhoven.reader;

import clangenhoven.model.Customer;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Converts a provided InputStream into a Stream of Customer objects by reading each line and
 * attempting to parse it as JSON into a Customer object. If parsing fails, the line is ignored.
 */
public class CustomerReader {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerReader.class);

    private static final ObjectMapper mapper = new ObjectMapper();

    private final BufferedReader bufferedReader;

    public CustomerReader(InputStream inputStream) {
        bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
    }

    private Iterable<String> toIterable(Iterator<String> iterator) {
        return () -> iterator;
    }

    private Optional<Customer> toCustomer(String string) {
        try {
            return Optional.of(mapper.readValue(string, Customer.class));
        } catch (IOException e) {
            LOGGER.warn("Failed to deserialize Customer from line: '" + string + "'", e);
            return Optional.empty();
        }
    }

    public Stream<Customer> stream() throws IOException {
        StringIterator it = new StringIterator(bufferedReader);
        return StreamSupport.stream(toIterable(it).spliterator(), false)
                .map(this::toCustomer)
                .filter(Optional::isPresent)
                .map(Optional::get);
    }
}
